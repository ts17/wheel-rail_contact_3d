# -*- coding: utf-8 -*-
"""A library for the preprocess.py.

Convention.
In the following code the adopted naming convention is the one of PEP 8
(see https://www.python.org/dev/peps/pep-0008/#naming-conventions) with
the exception that the function and method names are mixedCase. 

Rostyslav Skrypnyk.
"""
# NOTE! All the output messages (e.g. from print statements) can be found
# in .rpy file.

# Standard library imports:
import math
# 3rd party library imports:
from abaqus import *
from abaqusConstants import *
import regionToolset as rt
import mesh
# local library specific imports:
import settings as s


def createRailPart():
    """Creates a rail part and its sets.

    Rostyslav Skrypnyk.
    """
    print '/// Creating rail part.'

    m = mdb.models.values()[0]

    rail_sketch = m.ConstrainedSketch(name='rail', sheetSize=200.0)

    rail_sketch.Spline(s.rail_coords)
    rail_sketch.Line(s.rail_coords[0],
                     (s.rail_coords[0][0], s.rail_coords[0][1]-s.edge_length))
    rail_sketch.Line((s.rail_coords[0][0], s.rail_coords[0][1]-s.edge_length),
                     (s.rail_coords[-1][0], s.rail_coords[0][1]-s.edge_length))
    rail_sketch.Line((s.rail_coords[-1][0], s.rail_coords[0][1]-s.edge_length),
                     s.rail_coords[-1])

    rail_part = m.Part(name='rail',
                       dimensionality=THREE_D,
                       type=DEFORMABLE_BODY)
    rail_part.BaseSolidExtrude(sketch=rail_sketch,
                               depth=0.5*s.edge_length)
    
    # Mark contact point:
    rail_part.DatumPointByCoordinate(coords=s.rail_contact_point)
    
    # Create sets:
    mid_x = 0.5*(s.rail_coords[0][0]+s.rail_coords[-1][0])
    mid_y = s.rail_coords[0][1] - 0.5*s.edge_length
    mid_z = 0.25*s.edge_length

    rail_part.Set(cells=rail_part.cells, name='all')
    rail_part.Set(faces=rail_part.faces, name='all_faces')
    rail_part.Set(faces=rail_part.faces.findAt( ((s.rail_contact_point[0],
                                                  s.rail_contact_point[1],
                                                  mid_z),) ),
                  name='top')
    rail_part.Set(faces=rail_part.faces.findAt( ((mid_x,
                                                  s.rail_coords[0][1]-s.edge_length,
                                                  mid_z),) ),
                  name='bottom')
    rail_part.Set(faces=rail_part.faces.findAt( ((mid_x,
                                                  mid_y,
                                                  s.rail_contact_point[2]),) ), 
                  name='symm') # Symmetry plane.
    rail_part.Set(faces=rail_part.faces.findAt( ((mid_x,
                                                  mid_y,
                                                  0.5*s.edge_length),) ),
                  name='back') # Face parallel (opposite) to symmetry plane.
    rail_part.Set(faces=(rail_part.faces.findAt( ((s.rail_coords[0][0],
                                                   mid_y,
                                                   mid_z),) ),
                         rail_part.faces.findAt( ((s.rail_coords[-1][0],
                                                   mid_y,
                                                   mid_z),) )),
                  name='sides')
    # The left edges if Z axis points in your direction:
    rail_part.Set(edges=rail_part.edges.findAt( ((s.rail_coords[0][0],
                                                  s.rail_coords[0][1],
                                                  mid_z),) ),
                  name='top_side_edge')
    rail_part.Set(edges=rail_part.edges.findAt( ((s.rail_coords[0][0],
                                                  s.rail_coords[0][1]-s.edge_length,
                                                  mid_z),) ),
                  name='bottom_side_edge')
    
    # Create a surface for the contact: rail_part.Surface(name='top',
    rail_part.Surface(name='top', side1Faces=rail_part.sets['top'].faces)
    
# End of createRailPart function.


def createWheelPart():
    """Creates a wheel part and its sets.

    Rostyslav Skrypnyk.
    """
    print '/// Creating wheel part.'

    m = mdb.models.values()[0]
    
    wheel_sketch = m.ConstrainedSketch(name='wheel', sheetSize=200.0)
    
    # Create an instance of construction line to revolve around:
    cline = wheel_sketch.ConstructionLine(point1=(-100., 0.), point2=(100., 0.))
    
    wheel_sketch.Spline(s.wheel_coords)
    wheel_sketch.Line(s.wheel_coords[0], (s.wheel_coords[0][0], 0.))
    wheel_sketch.Line((s.wheel_coords[0][0], 0.),
                      (s.wheel_coords[-1][0], 0.))
    wheel_sketch.Line((s.wheel_coords[-1][0], 0.), s.wheel_coords[-1])
    
    wheel_part = m.Part(name='wheel',
                        dimensionality=THREE_D,
                        type=DEFORMABLE_BODY)
    
    wheel_part.BaseSolidRevolve(sketch=wheel_sketch,
                                angle=90.0, # degrees.
                                flipRevolveDirection=OFF) # This is the correct
    # revolution direction w.r.t. the extrusion direction of the rail. It
    # ensures correct positioning of the profiles w.r.t. each other at the
    # assembly stage.
    
    # Prepare to get rid of the unnecessary material:
    wheel_part.DatumPointByCoordinate(coords=s.wheel_contact_point)
    
    wheel_part.DatumPlaneByPrincipalPlane(principalPlane=XZPLANE,
                                          offset=s.wheel_contact_point[1] + \
                                          s.edge_length)
    wheel_part.DatumPlaneByPrincipalPlane(principalPlane=XYPLANE,
                                          offset=s.wheel_contact_point[2] + \
                                          0.5*s.edge_length)
    
    wheel_part.PartitionCellByDatumPlane(datumPlane=wheel_part.datums[3],
                                         cells=wheel_part.cells)
    
    wheel_part.PartitionCellByDatumPlane(datumPlane=wheel_part.datums[4],
                                         cells=wheel_part.cells[1])
    
    faces_preserved = [wheel_part.faces[i] for i in \
                       wheel_part.cells[0].getFaces()]
    
    # Get rid of the unnecessary material:
    for cell in wheel_part.cells:
        wheel_part.RemoveFaces(faceList=[face for face in wheel_part.faces if \
                                         face not in faces_preserved],
                               deleteCells=False)
    
    # Create sets:
    mid_y = s.wheel_contact_point[1] + 0.5*s.edge_length
    mid_z = 0.25*s.edge_length
    
    wheel_part.Set(cells=wheel_part.cells, name='all')
    wheel_part.Set(faces=wheel_part.faces, name='all_faces')
    wheel_part.Set(faces=wheel_part.faces.findAt( ((s.wheel_contact_point[0],
                                                    mid_y,
                                                    s.wheel_contact_point[2]),) ), 
                   name='symm') # Symmetry plane.
    wheel_part.Set(faces=wheel_part.faces.findAt( ((s.wheel_contact_point[0],
                                                    mid_y,
                                                    0.5*s.edge_length),) ),
                   name='back') # Face parallel (opposite) to symmetry plane.
    wheel_part.Set(faces=wheel_part.faces.findAt( ((s.wheel_contact_point[0],
                                                    s.wheel_contact_point[1] +\
                                                    s.edge_length,
                                                    mid_z),) ), 
                   name='top')
    # Make use of the fact that the profile is revolved. A midpoint on the
    # bottom surface is, therefore, can be located with the help of the
    # Pythagorean theorem:
    index = int( math.ceil(0.5*len(s.wheel_coords)) )
    profile_mid_x, profile_mid_y = s.wheel_coords[index]
    bottom_centre_y = - math.sqrt(profile_mid_y**2 - mid_z**2)
    wheel_part.Set(faces=wheel_part.faces.findAt( ((profile_mid_x,
                                                    bottom_centre_y,
                                                    mid_z),) ), 
                   name='bottom')
    wheel_part.Set(faces=(wheel_part.faces.findAt( ((s.wheel_coords[0][0],
                                                     mid_y,
                                                     mid_z),) ),
                          wheel_part.faces.findAt( ((s.wheel_coords[-1][0],
                                                     mid_y,
                                                     mid_z),) )),
                   name='sides')

    # The right edges if Z axis points in your direction:
    wheel_part.Set(edges=wheel_part.edges.findAt( ((s.wheel_coords[-1][0],
                                                    s.wheel_contact_point[1] +\
                                                    s.edge_length,
                                                    mid_z),) ),
                   name='top_side_edge')
    bottom_side_y = - math.sqrt(s.wheel_coords[-1][1]**2 - mid_z**2)
    wheel_part.Set(edges=wheel_part.edges.findAt( ((s.wheel_coords[-1][0],
                                                    bottom_side_y,
                                                    mid_z),) ),
                   name='bottom_side_edge') 
    # Create a surface for the load:
    wheel_part.Surface(name='top',
                       side1Faces=wheel_part.sets['top'].faces)
    # Create a surface for the contact:
    wheel_part.Surface(name='bottom',
                       side1Faces=wheel_part.sets['bottom'].faces)
    
# End of createWheelPart function.


def partitionRail():
    """Partitions the rail part and creates additional sets.

    Rostyslav Skrypnyk.
    """
    print '/// Partitioning rail.'

    m = mdb.models.values()[0]
    rail_sketch = m.sketches['rail']
    rail_part = m.parts['rail']
    quarter_edge = 0.25*s.edge_length
    
    # Partition top surface.
    # Do a transformation of the view to make a sketch of the top surface:
    t = rail_part.MakeSketchTransform(sketchPlane=rail_part.sets['bottom'].faces[0],
                                      sketchPlaneSide=SIDE1,
                                      sketchUpEdge=rail_part.sets['bottom_side_edge'].edges[0])
    
    s1 = m.ConstrainedSketch(name='__profile__', sheetSize=200, transform=t)
    
    # Project top surface onto the bottom plane to be able to access the
    # location of the contact point:
    rail_part.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)
    
    # The origin is the middle of the sketch, therefore:
    top_left = (s1.vertices[4].coords[0] - quarter_edge, s.edge_length)
    bottom_right = (s1.vertices[4].coords[0] + quarter_edge, 0)
    
    s1.rectangle(point1=top_left, point2=bottom_right)
    
    rail_part.PartitionFaceBySketchThruAll(faces=rail_part.sets['top'].faces,
                                           sketchPlane=rail_part.sets['bottom'].faces[0],
                                           sketchPlaneSide=SIDE1,
                                           sketchUpEdge=rail_part.sets['top_side_edge'].edges[0],
                                           sketch=s1)
    
    # Partition front (symmetry) face.
    # The origin is NOT in the middle of the sketch.
    # Redefine corners of the partitioning rectangle:
    top_left = (s.rail_contact_point[0] - quarter_edge,
                s.rail_contact_point[1] + s.edge_length)
    bottom_right = (s.rail_contact_point[0] + quarter_edge,
                    s.rail_contact_point[1] - quarter_edge) 
    
    rail_sketch.rectangle(point1=top_left, point2=bottom_right)
    
    rail_part.PartitionFaceBySketch(faces=rail_part.sets['symm'].faces,
                                    sketch=rail_sketch)
    
    # Create sets:
    rail_part.Set(edges=rail_part.edges.findAt( ((s.rail_contact_point[0],
                                                  bottom_right[1],
                                                  s.rail_contact_point[2]),) ),
                  name='symm_partition')
    rail_part.Set(edges=rail_part.edges.findAt( ((s.rail_contact_point[0],
                                                  s.rail_contact_point[1],
                                                  s.rail_contact_point[2] +\
                                                  quarter_edge),) ),
                  name='top_partition')
    rail_part.Set(edges=rail_part.edges.findAt( (s.rail_contact_point,) ),
                  name='common_partition')

# End of partitionRail function.


def partitionWheel():
    """Partitions the wheel part and creates additional sets.

    Rostyslav Skrypnyk.
    """
    print '/// Partitioning wheel.'

    m = mdb.models.values()[0]
    wheel_sketch = m.sketches['wheel']
    wheel_part = m.parts['wheel']
    quarter_edge = 0.25*s.edge_length

    # Partition bottom surface.
    # Do a transformation of the view to make a sketch of the bottom surface:
    t = wheel_part.MakeSketchTransform(sketchPlane=wheel_part.sets['top'].faces[0],
                                       sketchPlaneSide=SIDE1,
                                       sketchUpEdge=wheel_part.sets['top_side_edge'].edges[0])

    s1 = m.ConstrainedSketch(name='__profile__', sheetSize=200, transform=t)

    # Project bottom surface onto the top plane to be able to access the
    # location of the contact point:
    wheel_part.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)

    # The origin is the middle of the sketch, therefore:
    top_left = (s1.vertices[4].coords[0] - quarter_edge, s.edge_length)
    bottom_right = (s1.vertices[4].coords[0] + quarter_edge, 0)

    s1.rectangle(point1=top_left, point2=bottom_right)

    wheel_part.PartitionFaceBySketchThruAll(faces=wheel_part.sets['bottom'].faces,
                                            sketchPlane=wheel_part.sets['top'].faces[0],
                                            sketchPlaneSide=SIDE1,
                                            sketchUpEdge=wheel_part.sets['top_side_edge'].edges[0],
                                            sketch=s1)

    # Partition front (symmetry) face.
    # The origin is NOT in the middle of the sketch.
    # Redefine corners of the partitioning rectangle:
    top_left = (s.wheel_contact_point[0] - quarter_edge,
                s.wheel_contact_point[1] + quarter_edge)
    bottom_right = (s.wheel_contact_point[0] + quarter_edge,
                    s.wheel_contact_point[1] - s.edge_length) 

    wheel_sketch.rectangle(point1=top_left, point2=bottom_right)

    wheel_part.PartitionFaceBySketch(faces=wheel_part.sets['symm'].faces,
                                     sketch=wheel_sketch)

    # Create sets:
    wheel_part.Set(edges=wheel_part.edges.findAt( ((s.wheel_contact_point[0],
                                                    top_left[1],
                                                    s.wheel_contact_point[2]),) ),
                   name='symm_partition')

    index = int( math.ceil(0.5*len(s.wheel_coords)) )
    profile_mid_x, profile_mid_y = s.wheel_coords[index]
    bottom_centre_y = - math.sqrt(profile_mid_y**2 - quarter_edge**2)

    wheel_part.Set(edges=wheel_part.edges.findAt( ((profile_mid_x,
                                                    bottom_centre_y,
                                                    s.wheel_contact_point[2] +\
                                                    quarter_edge),) ),
                   name='bottom_partition')
    wheel_part.Set(edges=wheel_part.edges.findAt( ((profile_mid_x,
                                                    profile_mid_y,
                                                    0),) ),
                   name='common_partition')

# End of partitionWheel function.


def createMaterials():
    """Creates materials.

    Rostyslav Skrypnyk.
    """
    print '/// Creating materials.'

    m = mdb.models.values()[0]

    mat_elast = m.Material(name='linear_elastic')
    mat_elast.Elastic(table=((s.young_modul, s.poisson_ratio), ))

# End of createMaterials function.


def assignSections():
    """Assigns sections to the geometries of wheel and rail.

    Rostyslav Skrypnyk.
    """
    print '/// Assigning sections.'

    m = mdb.models.values()[0]
    rail_part = m.parts['rail']
    wheel_part = m.parts['wheel']

    m.HomogeneousSolidSection(name='rail', material='linear_elastic',
                              thickness=s.thickness)
    m.HomogeneousSolidSection(name='wheel', material='linear_elastic',
                              thickness=s.thickness)
    
    rail_part.SectionAssignment(region=rt.Region(cells=rail_part.cells),
                                sectionName='rail')
    wheel_part.SectionAssignment(region=rt.Region(cells=wheel_part.cells),
                                 sectionName='wheel')

# End of assignSections function.


def meshRail():
    """Creates a mesh for the rail part.
    
    Rostyslav Skrypnyk.
    """
    print '/// Meshing rail.'

    m = mdb.models.values()[0]
    rail_part = m.parts['rail']

    rail_part.setMeshControls(regions=rail_part.cells,
                              elemShape=s.element_shape, 
                              technique=s.technique)

    rail_part.setElementType(regions=rt.Region(cells=rail_part.cells),
                             elemTypes=( mesh.ElemType(elemCode=s.element,
                                                       elemLibrary=STANDARD), ))

    # NOTE: it turns out the order of seeding matters. The latter seed has
    # a priority.
    rail_part.seedEdgeBySize(edges=rail_part.sets['symm_partition'].edges,
                             size=s.coef1*s.seed_size,
                             deviationFactor=0.1,
                             constraint=FINER)
    rail_part.seedEdgeBySize(edges=rail_part.sets['top_partition'].edges,
                             size=s.coef1*s.seed_size,
                             deviationFactor=0.1,
                             constraint=FINER)

    bottom_edges = [rail_part.edges[i] for i in \
                    rail_part.sets['bottom'].faces[0].getEdges()]
    rail_part.seedEdgeBySize(edges=bottom_edges,
                             size=s.coef2*s.seed_size,
                             deviationFactor=0.1,
                             constraint=FINER)

    side_edges_numbers_raw = [face.getEdges() for face in rail_part.sets['sides'].faces]
    # Join list of tuples into one list:
    side_edges_numbers = [number for edge_numbers in side_edges_numbers_raw \
                          for number in edge_numbers]
    side_edges = [rail_part.edges[i] for i in side_edges_numbers]
    
    rail_part.seedEdgeBySize(edges=side_edges,
                             size=s.coef2*s.seed_size,
                             deviationFactor=0.1,
                             constraint=FINER)

    rail_part.seedEdgeBySize(edges=rail_part.sets['common_partition'].edges,
                             size=s.seed_size,
                             deviationFactor=0.1,
                             constraint=FIXED)

    rail_part.generateMesh()

# End of meshRail function.


def meshWheel():
    """Creates a mesh for the wheel part.
    
    Rostyslav Skrypnyk.
    """
    print '/// Meshing wheel.'

    m = mdb.models.values()[0]
    wheel_part = m.parts['wheel']

    wheel_part.setMeshControls(regions=wheel_part.cells,
                               elemShape=s.element_shape, 
                               technique=s.technique)

    wheel_part.setElementType(regions=rt.Region(cells=wheel_part.cells),
                              elemTypes=( mesh.ElemType(elemCode=s.element,
                                                        elemLibrary=STANDARD), ))
    # NOTE: it turns out the order of seeding matters. The latter seed has
    # a priority.
    wheel_part.seedEdgeBySize(edges=wheel_part.sets['symm_partition'].edges,
                              size=s.coef1*s.seed_size,
                              deviationFactor=0.1,
                              constraint=FINER)
    wheel_part.seedEdgeBySize(edges=wheel_part.sets['bottom_partition'].edges,
                              size=s.coef1*s.seed_size,
                              deviationFactor=0.1,
                              constraint=FINER)

    top_edges = [wheel_part.edges[i] for i in \
                 wheel_part.sets['top'].faces[0].getEdges()]

    wheel_part.seedEdgeBySize(edges=top_edges,
                              size=s.coef2*s.seed_size,
                              deviationFactor=0.1,
                              constraint=FINER)

    side_edges_numbers_raw = [face.getEdges() for face in wheel_part.sets['sides'].faces]
    # Join list of tuples into one list:
    side_edges_numbers = [number for edge_numbers in side_edges_numbers_raw \
                          for number in edge_numbers]
    side_edges = [wheel_part.edges[i] for i in side_edges_numbers]
    
    wheel_part.seedEdgeBySize(edges=wheel_part.sets['sides'].edges,
                              size=s.coef2*s.seed_size,
                              deviationFactor=0.1,
                              constraint=FINER)

    wheel_part.seedEdgeBySize(edges=wheel_part.sets['common_partition'].edges,
                              size=s.seed_size,
                              deviationFactor=0.1,
                              constraint=FIXED)
    
    wheel_part.generateMesh()

# End of meshWheel function.


def assemble():
    """Creates an assembly of part instances.
    
    Rostyslav Skrypnyk.
    """
    print '/// Assembling.'

    m = mdb.models.values()[0]
    rail_part = m.parts['rail']
    wheel_part = m.parts['wheel']

    m.rootAssembly.DatumCsysByDefault(CARTESIAN)

    rail_inst = m.rootAssembly.Instance(name='rail', part=rail_part,
                                        dependent=ON)
    wheel_inst = m.rootAssembly.Instance(name='wheel', part=wheel_part,
                                         dependent=ON)
    
    # Move wheel towards the rail until contact points meet:
    wheel_inst.translate(vector=(rail_inst.datums[2].pointOn[0] - \
                                 wheel_inst.datums[2].pointOn[0],
                                 rail_inst.datums[2].pointOn[1] - \
                                 wheel_inst.datums[2].pointOn[1],
                                 0.0))

# End of assemble function.


def createStep():
    """Creates a step and its output requests.

    Rostyslav Skrypnyk.
    """
    print '/// Creating step.'

    m = mdb.models.values()[0]

    m.StaticStep(name='contact', previous='Initial', nlgeom=OFF,
                 maxNumInc=s.max_number_inc,
                 initialInc=s.initial_inc,
                 minInc=s.min_inc)
    if s.iterative_solver:
        m.steps['contact'].setValues(matrixSolver=ITERATIVE)
        
    # Delete unnecessary requests:
    del m.historyOutputRequests['H-Output-1'] # Not used anywhere.
    del  m.fieldOutputRequests['F-Output-1'] # Get rid of standard name.
    
    # Request output:
    a = m.rootAssembly
    fr1 = m.FieldOutputRequest(name='rail_all',
                               createStepName='contact',
                               variables=s.output_request_variables_all,
                               frequency=LAST_INCREMENT,
                               region=a.instances['rail'].sets['all'])
    
    fr2 = m.FieldOutputRequest(name='rail_bottom',
                               createStepName='contact',
                               variables=s.output_request_variables_bottom,
                               frequency=LAST_INCREMENT,
                               region=a.instances['rail'].sets['bottom'])
    
    # Restart request (allows terminated analysis to be restarted):
    m.steps['contact'].Restart(frequency=s.restart_frequency)

# End of createStep function.


def createInteraction():
    """Creates contact interaction and sets its properties. 

    Rostyslav Skrypnyk.
    """
    print '/// Creating interaction.'

    m = mdb.models.values()[0]
    a = m.rootAssembly

    # Contact properties:
    m.ContactProperty('exponential')
    mip1 = m.interactionProperties['exponential']
    mip1.NormalBehavior(pressureOverclosure=EXPONENTIAL,
                        table=((s.p0, 0.0), (0.0, s.c0)), # (pressure, clearance).
                        maxStiffness=None,
                        constraintEnforcementMethod=DEFAULT)
    # or:
    m.ContactProperty('hard')
    mip2 = m.interactionProperties['hard']
    mip2.NormalBehavior()
    
    # Actual contact:
    if s.hard_contact:
        print '/// HARD contact property was chosen.'
        inter_prop = 'hard'
    else:
        print '/// EXPONENTIAL contact property was chosen.'
        inter_prop = 'exponential'

    m.SurfaceToSurfaceContactStd(name='contact',
                                 createStepName='Initial',
                                 master=a.instances['rail'].surfaces['top'],
                                 slave=a.instances['wheel'].surfaces['bottom'],
                                 sliding=FINITE,
                                 interactionProperty=inter_prop)

# End of createInteraction function.


def setBC():
    """Creates and sets boundary conditions.

    Rostyslav Skrypnyk.
    """
    print '/// Setting boundary conditions.'

    m = mdb.models.values()[0]
    a = m.rootAssembly
    r = a.instances['rail']
    w = a.instances['wheel']
    m.DisplacementBC(name='ground', createStepName='Initial', 
                     region=r.sets['bottom'], 
                     u2=SET)
    
    rail_region = r.sets['bottom_side_edge']

    wheel_region = a.Set(edges=w.edges.findAt(w.sets['bottom_side_edge'].edges[0].pointOn,
                                              w.sets['top_side_edge'].edges[0].pointOn),
                         name='wheel_side_edges')
    
    m.DisplacementBC(name='no_rigid_body_motion_rail', 
                     createStepName='Initial', 
                     region=rail_region,
                     u1=SET,
                     u3=SET)
    m.DisplacementBC(name='no_rigid_body_motion_wheel', 
                     createStepName='Initial', 
                     region=wheel_region,
                     u1=SET,
                     u3=SET)
    m.ZsymmBC(name='z_symm_rail',createStepName='Initial',region=r.sets['symm'])
    m.ZsymmBC(name='z_symm_wheel',createStepName='Initial',region=w.sets['symm'])
    m.Pressure(name='pressure',
               createStepName='contact', 
               region=w.surfaces['top'], 
               magnitude=s.force/s.edge_length/s.edge_length, 
               distributionType=UNIFORM)

# End of setBC function.


def createJob():
    """Creates a job.

    Rostyslav Skrypnyk.
    """
    print '/// Creating job.'

    m = mdb.models.values()[0]
    
    mdb.Job(name=s.job_name,
            model=m.name,
            type=ANALYSIS,
            explicitPrecision=SINGLE,
            nodalOutputPrecision=SINGLE,  
            numDomains=s.n_domains,
            numCpus=s.n_cpus,
            memory=s.memory, 
	    memoryUnits=MEGA_BYTES,
            userSubroutine=s.umat,
            echoPrint=OFF, modelPrint=OFF, contactPrint=OFF, historyPrint=OFF)

# End of createJob function.


def runJob():
    """Submits the job for analysis.

    Rostyslav Skrypnyk.
    """
    print '/// Running job.'
    
    mdb.jobs.values()[0].submit()

# End of runJob function.


def allButRunJob():
    """Runs all the routines to create the analysis, but does not start it.

    Rostyslav Skrypnyk.
    """
    createRailPart()
    partitionRail()
    createWheelPart()
    partitionWheel()
    createMaterials()
    assignSections()
    meshRail()
    meshWheel()
    assemble()
    createStep()
    createInteraction()
    setBC()
    createJob()

# End of allButRunJob function.

