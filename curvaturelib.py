# -*- coding: utf-8 -*-
"""Library of routines to compute curvature of the wheel/rail profile.

Convention.
In the following code the adopted naming convention is the one of PEP 8
(see https://www.python.org/dev/peps/pep-0008/#naming-conventions) with
the exception that the function and method names are mixedCase. 

Rostyslav Skrypnyk.
"""

# Standard library imports:
import warnings
# 3rd party library imports:
import numpy as np
import scipy.optimize
# local library specific imports:


def getRegionToFit(point_array, point, span):
    """Returns a region of point array.

    The region consists of points located not farther than one span from
    the specified point.

    Arguments:
    point_array -- list of X,Y pairs.
    point -- X,Y coordinates of the point.
    span -- size of the region to one side of the point, [in units of
            coordinates].

    Rostyslav Skrypnyk.
    """
    return [p for p in point_array if (p[0] >= point[0] - span and \
                                       p[0] <= point[0] + span)]

# End of function getRegionToFit.


def leastSqError(coordinates,point):
    """Returns error according to least squares method.

    Error is the variance of the distance from the point to all X,Y pairs.
    
    Arguments:
    coordinates -- list of X,Y of the geometry.
    point -- coordinate of the point the distance is estimated to.
    
    Rostyslav Skrypnyk.
    """
    x1 = np.array( [x[0] for x in coordinates] )
    x2 = np.array( [x[1] for x in coordinates] )
    distance = list(np.sqrt((x1-point[0])**2 + (x2-point[1])**2))
    return np.var(distance)

# End of function leastSqError.


def findCircleCenter(point_array):
    """Returns the center of a circle fitted to the input geometry.
    
    Fits a circle to the part of the curve defined by the bounds and
    returns the position of its center.

    Arguments:
    point_array -- list of X,Y pairs of the curve.

    Rostyslav Skrypnyk.
    """
    # Approximate midpoint of the arc:
    point = point_array[int( 0.5*len(point_array) )]
    
    # Compute a starting guess for the circle center, based on the three points
    # (two corner points and the contact point in the middle)
    # (from Wolfram MathWorld):
    x1, y1 = point_array[0][0], point_array[0][1]
    x2, y2 = point
    x3, y3 = point_array[-1][0], point_array[-1][1]

    a = np.linalg.det( np.array([[x1, y1, 1], [x2, y2, 1], [x3, y3, 1]]) )
    d = - np.linalg.det( np.array([[x1*x1 + y1*y1, y1, 1], \
                                   [x2*x2 + y2*y2, y2, 1], \
                                   [x3*x3 + y3*y3, y3, 1]]) )
    e = np.linalg.det( np.array([[x1*x1 + y1*y1, x1, 1], \
                                 [x2*x2 + y2*y2, x2, 1], \
                                 [x3*x3 + y3*y3, x3, 1]]) )
    
    if abs(a) == 0: # Catch exception: division by zero.
        warnings.warn('Flat plane. Setting center of the circle at infinity.')
        inf = float('inf')
        return (inf,inf)
        
    x0 = - d / 2.0 / a
    y0 = - e / 2.0 / a
    center = np.array([x0,y0])

    # Apply minimization procedure to fit the circle to the arc:
    center = scipy.optimize.minimize(lambda center: \
                                     leastSqError(point_array,center),center,
                                     method='BFGS',tol=1e-12)
    
    return center.x

# End of function findCircleCenter.


def computeCurvatures(coordinates, x_range=1):
    """Returns curvatures at each point of the input geometry.

    Arguments:
    coordinates -- list of X,Y coordinates of the analyzed geometry.
    x_range -- half of the region the curvature is computed on, [in units of
               coordinates] (default: 1 [mm]).

    Rostyslav Skrypnyk.
    """
    curvatures = []
    for dot in coordinates:
        region = getRegionToFit(coordinates, dot, x_range)
        c0 = findCircleCenter(region)

        # Decide on the sign of the curvature:
        if c0[1] <= dot[1]: # If region is concave:
            sign = -1
        else: # Convex:
            sign = 1

        curvatures.append( sign/np.sqrt((dot[0] - c0[0])**2 + \
                                       (dot[1] - c0[1])**2) )

    return curvatures

# End of function computeCurvatures.
