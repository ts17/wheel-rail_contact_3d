# Wheel-rail contact 3D
Repository contains Python and Bash scripts for creating and running Abaqus 
jobs for 3D contact problems in Railway Mechanics.

# Acknowledgement 
The scripts are written based on the code of [Mikael Öhman](https://github.com/Micket). 
His contribution is highly appreciated.

# Note!
- *'hard'* pressure-overclosure type in **settings.py** led to lower computational time.
  However, there might be a convergence problem with it. In such case, choose *'exponential'*
  type by setting `hard_contact = False`.
- In **settings.py**: creation of sets often fails if contact point is not given with high
  enough precision. The workaround is to create a part and take the mid edge point using
  Query in Abaqus and use it as `rail_contact_point`.

# References
The following sources were used:

- Abaqus 6.13 Documentation Collection.
- [Learn Abaqus script in one hour](https://www.researchgate.net/publication/281295210_Learn_Abaqus_Script_in_One_Hour)
by Johannes T. B. Overvelde.

Rostyslav Skrypnyk.
