# -*- coding: utf-8 -*-
"""Prepares geometries of the wheel and rail profiles that can later be used
to create a spline in Abaqus.

Convention.
In the following code the adopted naming convention is the one of PEP 8
(see https://www.python.org/dev/peps/pep-0008/#naming-conventions) with
the exception that the function and method names are mixedCase. 

Rostyslav Skrypnyk.
"""

# Standard library imports:
import fileinput
import math
import os
# 3rd party library imports:
import numpy as np
import scipy.interpolate as spi
import matplotlib.pyplot as plt # Make sure it is installed on the machine.
# local library specific imports:


def circularArcPoints(radius, n_points=100, distance=None,
                      orientation_down=True, offset_y=0):
    """Returns a list of tuples with coordinates of the circular arc.

    Arguments:
    radius -- radius of the circle.
    n_points -- number of points that constitute the arc (default=100).
    distance -- half a span of the arc, i.e. the arc is created on the
                interval [-distance, distance] (default: None, which is later 
                substituted with radius).
    orientation_down -- orientation of the arc. Boolean (default: True).
    offset_y -- offset of Y coordinates, [in units of the coordinates]. Used
                for the wheel coordinates and equals to the wheel's radius.
                Default: 0.

    Rostyslav Skrypnyk.
    """
    if distance is None:
        distance = float(radius)
    else: 
        distance = float(distance) # Guard against integer division.
    
    if distance > radius:
        raise ValueError('Half-span of the arc cannot be larger than radius.')
    
    if orientation_down:
        sign = -1
    else:
        sign = 1

    coords = []
    for point in range(n_points+1): # +1 needed to complete the arc.
        # x coordinates from left to right (important for creating sets
        # in Abaqus):
        x = math.cos(math.acos(distance/radius) + 2*math.asin(distance/radius) \
                     * (n_points-point) / n_points)*radius
        y = math.sin(sign*(math.acos(distance/radius) + \
                           2*math.asin(distance/radius)*point/n_points))*radius
        # Offset if orientation_down is True:
        coords.append( (x, y + 0.5*(sign - 1)*(offset_y - radius)) )

    return coords

# End of circularArcPoints function.


def ellipticArcPoints(x_axis, y_axis, n_points=100, distance=None,
                      orientation_down=True, offset_y=0):
    """Returns a list of tuples with coordinates of the elliptic arc.

    Arguments:
    x_axis -- size of the semi-axis along X.
    y_axis -- size of the semi-axis along Y.
    n_points -- number of points that constitute the arc.
    distance -- half a span of the arc, i.e. the arc is created on the
                interval [-distance, distance] (default: None, which is later 
                substituted with size of the X semi-axis).
    orientation_down -- orientation of the arc. Boolean (default: True).
    offset_y -- offset of Y coordinates, [in units of the coordinates]. Used
                for the wheel coordinates and equals to the wheel's radius.
                Default: 0.

    Rostyslav Skrypnyk.
    """
    if distance is None:
        distance = float(x_axis)
    else: 
        distance = float(distance) # Guard against integer division.
    
    if distance > radius:
        raise ValueError('Half-span of the arc cannot be larger than radius.')
    
    if distance > x_axis:
        raise ValueError( ('Half-span of the arc cannot be ' \
                           'larger than the X semi-axis.') )

    if orientation_down:
        sign = -1
    else:
        sign = 1

    coords = []
    R = math.sqrt( distance**2 + (y_axis**2)*(1 - (distance/x_axis)**2) )
    for point in range(n_points+1): # +1 needed to complete the arc.
        # fraction = (n_points - point) / n_points
        # x coordinates from left to right (important for creating sets
        # in Abaqus):
        fraction = (n_points - point) / float(n_points)
        x = math.cos( math.acos(distance/R) + \
                      2*math.asin(distance/R) * fraction ) * R
        y = sign * y_axis * math.sqrt( 1 - (x/x_axis)**2 )
        coords.append( (x, y + 0.5*(sign - 1)*(offset_y - y_axis)) )

    return coords

# End of semiEllipsePoints function.


def rotateGeometry(geometry, angle):
    """Returns rotated geometry.

    Arguments:
    geometry -- list of X,Y pairs.
    angle -- angle of rotation in degrees.

    Rostyslav Skrypnyk.
    """
    theta = (angle/180.) * np.pi

    rotation_matrix = np.array( [[np.cos(theta), -np.sin(theta)],
                                 [np.sin(theta), np.cos(theta)]] )

    rotated_geom = []
    
    for point in geometry:
        rotated_geom.append( tuple(np.dot(rotation_matrix, point)) )

    return rotated_geom

# End of rotateGeometry function.


def trimAroundBottom(geometry, distance, return_extremum=False):
    """Locates the lowest point in the geometry and returns a trimmed part of it.

    Arguments:
    geometry -- list of X,Y pairs.
    distance -- distance to left and right from the extremum to trim at.
    return_extremum -- boolean for requesting additional output.
                       Default: False.

    Rostyslav Skrypnyk.
    """
    extremum_y = float('inf')

    for point in geometry:
        if point[1] < extremum_y:
            extremum_x = point[0]
            extremum_y = point[1]

    trimmed_geom = []
    for point in geometry:
        if (point[0] >= extremum_x - distance and \
            point[0] <= extremum_x + distance):
            trimmed_geom.append(point)

    if return_extremum:
        return trimmed_geom, (extremum_x, extremum_y)
    else:
        return trimmed_geom

# End of function trimAroundBottom.


def plotGeometry(geometry):
    """Plots input data. Supplementary routine for visualization purposes.

    Arguments:
    geometry -- list of tuples with coordinates to plot.

    Rostyslav Skrypnyk.
    """
    
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    ax1.grid()

    x = [point[0] for point in geometry]
    y = [point[1] for point in geometry]

    ax1.plot(x,y)
    plt.show()

# End of plotGeometry function.


def loadTrimGeometry(path, contact_point_x, distance, offset_y=0):
    """Loads geometry and returns a 'trimmed' part of it.

    Gets rid of points that are farther from contact point than
    the specified distance. Returns a list of tuples. Each tuple is
    a pair of x,y coordinates.

    Arguments:
    path -- path to the file containing geometry.
    contact_point_x -- X coordinate of the contact point.
    distance -- size of the region to trim on one side of the contact 
                point, [in units of the coordinates].
    offset_y -- offset of Y coordinates, [in units of the coordinates]. Used
                for the wheel coordinates and equals to the wheel's radius.
                Default: 0.

    Rostyslav Skrypnyk.
    """
    coords = []
    for line in fileinput.input(path):
        if (not line.startswith('#')) and line.strip():
            xy_pair = map(float, line.split())
            if (xy_pair[0] >= contact_point_x - distance) and \
               (xy_pair[0] <= contact_point_x + distance):
                coords.append( (xy_pair[0],
                                -(xy_pair[1] + offset_y)) ) # Flip y coordinate.

    return coords

# End of loadTrimGeometry function.


def correctRail(geometry, save=False, file_name='rail_corrected'):
    """Returns rectified rail profile and optionally saves it into a file.

    The rail profile might contain an artefact in the form of the flat surface
    on the rail head. This function applies interpolation to the horizontal
    region with a maximum Y coordinate.

    Arguments:
    geometry -- array of coordinates.
    save -- boolean (default: False). If True, corrected geometry will be saved
            to a file.
    file_name -- string to represent the name of a file corrected geometry is
                 saved to (default: rail_corrected).
    
    Rostyslav Skrypnyk.
    """
    x, y = zip(*geometry) # Inverse of zip().
    
    y_max = max(y)

    x_part, y_part = [], []
    for point in geometry:
        if point[1] < y_max:
            x_part.append(point[0])
            y_part.append(point[1])
    
    f = spi.interp1d(x_part, y_part, kind='cubic')

    geometry_new = zip(x, f(x))

    if save:
        file_path = os.getcwd() + '/input/'
        text = 'Corrected geometry using interpolation.'
        
        np.savetxt(file_path + file_name + '.rail',
                   zip(x, -f(x)), # Minus sign inserted for consistency with
                   # original profile.
                   fmt='%.6e', delimiter='   ', header=text)

    return geometry_new

# End of correctRail function.


############################################# For comparison of FEM and CONTACT:
# half_edge_length = 20
# rail_region = circularArcPoints(radius=120,
#                                 distance=half_edge_length,
#                                 orientation_down=False)
# # wheel_region = circularArcPoints(radius=300,
# #                                  distance=half_edge_length,
# #                                  offset_y=radius)
# wheel_region = ellipticArcPoints(x_axis=40, y_axis=120,
# #                                 distance=half_edge_length,
#                                  offset_y=radius)

# temp_wheel, cp_w = trimAroundBottom(rotateGeometry(wheel_region[:int(0.5*len(wheel_region)+1)],
#                                                    angle=30),
#                                     distance=half_edge_length,
#                                     return_extremum=True)
# wheel_region = [point for point in temp_wheel if point[1] <= -398]

# # plotGeometry(wheel_region)

# contact_point_x_rail = 0.0
# contact_point_x_wheel = cp_w[0]

# # path = '/Users/skrypnyk/Box Sync/ts17/paper A/for local 3d contact simulation/CONTACT_TS17/PRE/PROFILES/'
# # name = 'tilted_elliptic_arc.m'
# # wheel_region_CONTACT = [(point[0]-cp_w[0],point[1]-cp_w[1]) for point in wheel_region]
# # np.savetxt(path+name,wheel_region_CONTACT, delimiter='     ')
