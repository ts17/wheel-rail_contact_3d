# -*- coding: utf-8 -*-
"""Settings for the preprocess.py.

Convention.
In the following code the adopted naming convention is the one of PEP 8
(see https://www.python.org/dev/peps/pep-0008/#naming-conventions) with
the exception that the function and method names are mixedCase. 

Rostyslav Skrypnyk.
"""

# Standard library imports:

# 3rd party library imports:
from abaqusConstants import *
# local library specific imports:
import profilegeometry as pg

#################################### Part ######################################
rail_path = './input/SEC026_diverging_l_corrected.rail'
wheel_path = './input/S1002.wheel'
radius = 460 # [mm].

# Lateral displacement dY = 0.0 mm:
# Creation of sets often fails if contact point is not given with high
# enough precision. The workaround is to create a part and take the middle
# edge point using Query in Abaqus and paste it here:
rail_contact_point = (-1.3710020322580643, -1.1500053449010224, 0.0)
# Apply linear interpolation to find Y coordinate of the contact point:
wheel_contact_point = (-1.3740589333333332,
                       -radius+4.326642645633459e-02,
                       0.0)

edge_length = 60. # Edge of the cut-out cube in the wheel/rail,
# [in coordinate units].

rail_coords = pg.loadTrimGeometry(rail_path,
                                  rail_contact_point[0],
                                  0.5*edge_length)
wheel_coords = pg.loadTrimGeometry(wheel_path,
                                   wheel_contact_point[0],
                                   0.5*edge_length,
                                   offset_y=radius)

################################## Material ####################################
young_modul = 1.83E5 # [MPa].
poisson_ratio = 0.3

################################## Section #####################################
thickness = 1. # Cross-section thickness.

#################################### Mesh ######################################
element_shape = TET
element = C3D4 # 4-node linear tetrahedron.
technique = FREE
seed_size = 0.25 # [in coordinate units].
coef1 = 20 # Seed size coefficient. Will be multiplied by seed_size.
coef2 = 60

#################################### Step ######################################
max_number_inc = int(1e4) # Maximum number of increments.
initial_inc = 2.5e-3 # Initial increment.
min_inc=1.e-5 # Minimum increment allowed.
iterative_solver = False # Only applicable to models with 3D elements.
# Request output variables in the whole part:
output_request_variables_all = ('MISESMAX', 'CSTRESS', 'U')
# and in the certain region:
output_request_variables_bottom = ('RF',)
restart_frequency = 10 # Integer specifying increments at which restart
# information will be written. 

################################## Interaction #################################
hard_contact = True # 'hard' if True, 'exponential' if False. Boolean to
# reflect on pressure-overclosure type. 'Hard' was ~4 times faster in tests!

# Only for exponential pressure-overclosure:
p0 = 10 # Pressure at zero clearance.
c0 = 0.001 # Clearance at zero pressure.

############################ Boundary conditions ###############################
force = 1.115e5 # Applied force, [N].

#################################### Job #######################################
job_name = 'wheel-rail_contact_3D'
n_cpus=2 # Number of CPUs.
n_domains=2 # Number of domains (Abaqus/Explicit only). Must be a multiple
# of n_cpus.
memory=512 # Memory available to Abaqus analysis, [megabytes].
umat = '' # A string specifying the file containing user's subroutine
# definition. The default value is an empty string.
