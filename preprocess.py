# -*- coding: utf-8 -*-
"""Preprocess program that creates an input file for Abaqus. 

Convention.
In the following code the adopted naming convention is the one of PEP 8
(see https://www.python.org/dev/peps/pep-0008/#naming-conventions) with
the exception that the function and method names are mixedCase. 

Rostyslav Skrypnyk.
"""
# NOTE! All the output messages (e.g. from print statements) can be found
# in .rpy file.

# Standard library imports:

# 3rd party library imports:

# local library specific imports:
import preprocesslib as lib

lib.allButRunJob()
