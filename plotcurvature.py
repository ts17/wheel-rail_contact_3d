# -*- coding: utf-8 -*-
"""A script to plot curvatures.

Convention.
In the following code the adopted naming convention is the one of PEP 8
(see https://www.python.org/dev/peps/pep-0008/#naming-conventions) with
the exception that the function and method names are mixedCase. 

Rostyslav Skrypnyk.
"""

# Standard library imports:
import os
# 3rd party library imports:
import matplotlib.pyplot as plt
import numpy as np
# local library specific imports:
import profilegeometry as pg
import curvaturelib as clib


plt.rcParams.update({'axes.labelsize' : 16,
                     'font.size'      : 18,
                     'legend.fontsize': 18,
                     'xtick.labelsize': 16,
                     'ytick.labelsize': 16})


def checkCreateDirectory(path):
    """Checks if the directory already exists, otherwise creates it.

    Arguments:
    path -- string containing path to the directory including its name.

    Rostyslav Skrypnyk.
    """
    # Create directory, if it does not exist:
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except OSError as exc: # Guard against race condition.
            if exc.errno != errno.EEXIST:
                raise

# End of function checkCreateDirectory.


def saveData(data, file_name, bounds=[]):
    """Saves data into a file.

    Arguments:
    data -- data to be saved to a file.
    file_name -- string containing a name of the file the data
                 is to be saved in.
    bounds -- a sequence to reflect on the bounds of the contact zone.

    Rostyslav Skrypnyk.
    """
    file_path = os.getcwd() + '/data/'
    checkCreateDirectory(file_path)
    
    preamble = ('Convention: positive curvature denotes' +
                ' geometry is a convex function, negative -- concave.\n')
    if bounds: # If not empty sequence:
        preamble = preamble + ('Potential contact zone lies' +
                               ' in the interval [%f, %f]' +
                               ' (for the range of lateral displacement dy' +
                               ' [-5, 5] mm).\n') % bounds

    text_header = preamble + 'Coordinate, [mm]   Curvature, [1/m]'

    np.savetxt(file_path + file_name + '.dat', data,
               fmt='%.6e', delimiter='\t\t',
               header=text_header)

# End of function saveData.


def createPlot(x, curvature, 
               x_range=None, y_max=None, display=True,
               save_fig=False, save_data=False, 
               file_name='curvature', ext='png',
               contact_bounds=[], uic60=False):
    """Plots curvature against coordinate.

    Optionally saves the plot.

    Arguments:
    x -- list of X coordinates.
    curvature -- list of curvatures.
    x_range -- absolute value of lower and upper limits of X
               axis (default: None).
    y_max -- limit the plot along Y axis (default: None).
    display -- boolean (default: True). If True, displays the plot to a screen.
    save_fig -- boolean (default: False). If True, saves plot to a file.
    save_data -- boolean (default: False). If True, saves data into a file.
    file_name -- string to represent the name of the file the figure or data
                 is saved as (default: curvature).
    ext -- a string stating an extension of the file the plot is saved
           to (default: png; options: pdf, png, ps, eps, svg).
    contact_bounds -- a pair of coordinates that denotes lower and upper
                      bounds of the contact zone (default: empty sequence).
    uic60 -- boolean to reflect on whether geometry represents the UIC60 rail
             profile (default: False).
    
    Rostyslav Skrypnyk.
    """
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    ax1.grid()

    ax1.plot(x,curvature)

    ax1.set_xlabel(r'y, [mm]')
    ax1.set_ylabel(r'Curvature ($1/R$), [m${}^{-1}$]')
    
    if x_range is not None:
        plt.xlim([-x_range, x_range])
    
    if y_max is not None:
        plt.ylim(ymax=y_max)
    
    if uic60: # Start uic60 section:
        rail_top = (-3.5, 0) # From uic60i00.rail file.
        # From EN 13674-1:2003(E) standard:
        r300_span = 0.5*20.456 # Span of the zone with curvature radius 300 mm.
        r80_span = 0.5*52.053 # Same for radius 80 mm.
        r13_span = 0.5*72 # Same for radius 13 mm.

        # Draw auxiliary horizontal lines:
        ax1.axhline(y=-1.0/300, color='r', linestyle='dashdot')
        ax1.axhline(y=-1.0/80, color='m', linestyle='dashdot')
        ax1.axhline(y=-1.0/13, color='g', linestyle='dashdot')

        ax1.annotate(r'$R = 300$', xy=(-20,-1./300), 
                     xytext=(-50,20), textcoords='offset points',
                     arrowprops=dict(arrowstyle='simple',fc='black'))
        ax1.annotate(r'$R = 80$', xy=(-20,-1./80),
                     xytext=(-50,20), textcoords='offset points',
                     arrowprops=dict(arrowstyle='simple',fc='black'))
        ax1.annotate(r'$R = 13$', xy=(-20,-1./13),
                     xytext=(15,20), textcoords='offset points',
                     arrowprops=dict(arrowstyle='simple',fc='black'))

        # Draw auxiliary vertical lines:
        ax1.axvline(x=7.0, color='r', linestyle='dashdot')
        ax1.axvline(x=22.5, color='m', linestyle='dashdot')
        ax1.axvline(x=31.8, color='g', linestyle='dashdot')

        ax1.annotate(r'$R = 300$', xy=(7,-0.03),
                     xytext=(-70,-30), textcoords='offset points',
                     arrowprops=dict(arrowstyle='simple',fc='black'))
        ax1.annotate(r'$R = 80$', xy=(22.5,-0.03),
                     xytext=(-70,-30), textcoords='offset points',
                     arrowprops=dict(arrowstyle='simple',fc='black'))
        ax1.annotate(r'$R = 13$', xy=(31.8,-0.03),
                     xytext=(-30,-30), textcoords='offset points',
                     arrowprops=dict(arrowstyle='simple',fc='black'))
    # End of uic60 section.

    if contact_bounds: # If not empty sequence:
        # Mark potential contact zone:
        ax1.axvspan(contact_bounds[0],contact_bounds[1],
                    color='grey', alpha=0.3, linewidth=0)
        x_min, x_max = ax1.get_xlim()
        x_axis_length = sum(np.absolute([x_min, x_max]))
        contact_zone_middle_normalized = (abs(x_min) + 0.5*sum(contact_bounds))\
                                         /x_axis_length
        ax1.text(x=contact_zone_middle_normalized,
                 y=0.5,s=r'potential contact zone',
                 horizontalalignment='center',
                 verticalalignment='center',
                 rotation='vertical',
                 transform=ax1.transAxes)
    
    if save_fig:
        file_path = os.getcwd() + '/img/'
        checkCreateDirectory(file_path)
        plt.savefig(file_path + file_name + '.' + ext,
                    bbox_inches='tight',dpi=800, transparent=True)

    if save_data:
        saveData(np.array([x, curvature]).T, file_name, contact_bounds)

    if display:
        plt.show()

# End of function createPlot.


################################# Start the script #############################
span=25
# coords = pg.ellipticArcPoints(x_axis=40, y_axis=120,
#                               n_points=200,# distance=span,
#                               offset_y=pg.radius)
# coords = pg.loadTrimGeometry(path='./input/S1002.wheel',
#                              contact_point_x=-16.792,
#                              distance=span)
# raw_coords = pg.trimAroundBottom(pg.rotateGeometry(coords[:int(0.5*len(coords)+1)],
#                                                    angle=30),
#                                  distance=span)
# coords = [point for point in raw_coords if point[1] <= -398]

rail_center = -5.4763 # section 24: 7.4054; section 25: -3.495e-1; section 26: -5.4763
wheel_center = 0

section = 26

coords = pg.loadTrimGeometry(path='./input/S1002.wheel',
                             contact_point_x=wheel_center,
                             distance=span)
# coords = pg.loadTrimGeometry(path='./input/SEC0'+str(section)+'_diverging_l_corrected.rail',
#                              contact_point_x=rail_center,
#                              distance=span)
# coords = pg.correctRail(coords, save=True,
#                         file_name='SEC0'+str(section)+'_diverging_l_corrected')

# From the .kpfr corresponding to the geometry file:
# Linear interpolation to get the value at dy = -5 mm:
rail_lower_bound = (3.305 - 3.826)*(3.62 - 5)/(3.62 - 5.36) - 3.305 
wheel_lower_bound = (10.304 - 8.53)*(6.34 - 5)/(6.34 - 4.82) - 10.304
# Same for dy = 5 mm:
rail_upper_bound =  9.696 + (14.249 - 9.696)*(5 - 4.8)/(6.14 - 4.8)
wheel_upper_bound = 15.37 + (20.25 - 15.37)*(5 - 4.98)/(6.12 - 4.98)

plot_geom = False
if plot_geom:
    pg.plotGeometry(coords)
    
curvs = np.array(clib.computeCurvatures(coords, x_range=1)) * 1e3
x = [c[0] for c in coords]
createPlot(x, curvs, x_range=None, y_max=None, display=True,
           save_fig=False, save_data=True, ext='pdf',
           file_name='curvature-S1002-distance25-1mmRange',
#           file_name='curvature-sec'+str(section)+'_corrected-distance25-1mmRange',
           uic60=False,
           contact_bounds=())# (rail_lower_bound, rail_upper_bound))
